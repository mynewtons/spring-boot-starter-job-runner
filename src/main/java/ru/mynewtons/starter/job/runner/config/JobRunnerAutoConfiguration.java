package ru.mynewtons.starter.job.runner.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.mynewtons.starter.job.runner.client.WorkflowClient;

import javax.sql.DataSource;

@Configuration
@ComponentScan(basePackages = "ru.mynewtons.starter.job.runner")
@ConditionalOnClass(DataSource.class)
public class JobRunnerAutoConfiguration {

    @Value("${starter.job.runner.workflow.url}")
    private String workflowUrl;

    @Bean
    public WorkflowClient workflowClient(){
        return new WorkflowClient(workflowUrl);
    }
}
