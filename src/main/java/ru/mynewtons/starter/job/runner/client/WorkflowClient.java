package ru.mynewtons.starter.job.runner.client;

import feign.*;
import feign.gson.GsonDecoder;
import feign.gson.GsonEncoder;
import feign.okhttp.OkHttpClient;
import feign.slf4j.Slf4jLogger;
import org.springframework.stereotype.Component;


public class WorkflowClient {

    protected FeignClient feignClient;

    public WorkflowClient(String url){
        this.feignClient = Feign.builder()
                .client(new OkHttpClient())
                .encoder(new GsonEncoder())
                .decoder(new GsonDecoder())
                .logger(new Slf4jLogger(FeignClient.class))
                .logLevel(Logger.Level.FULL)
                .target(FeignClient.class, url);
    }

    public void taskComplete(String id, Object data){
        this.feignClient.taskComplete(id,data);
    }

    protected interface FeignClient{

        @RequestLine("POST /{id}")
        @Headers("Content-Type: application/json")
        void taskComplete(@Param("id") String id, Object data);
    }
}
