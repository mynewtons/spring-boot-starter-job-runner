package ru.mynewtons.starter.job.runner;

import ru.mynewtons.starter.job.runner.stereotype.Runner;

@Runner("simple")
public class SimpleRunner implements Runnable<String,SimpleInput> {
    @Override
    public String execute(SimpleInput input) {
        return input.getValue() + " world";
    }
}
