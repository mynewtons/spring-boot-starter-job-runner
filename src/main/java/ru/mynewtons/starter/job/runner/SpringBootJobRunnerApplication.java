package ru.mynewtons.starter.job.runner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootJobRunnerApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootJobRunnerApplication.class, args);
    }
}
