package ru.mynewtons.starter.job.runner;

public interface Input<T> {
    T getValue();
}
