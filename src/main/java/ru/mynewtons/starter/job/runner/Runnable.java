package ru.mynewtons.starter.job.runner;

import java.util.List;

public interface Runnable<T,K> {
    T execute(K input);
}
