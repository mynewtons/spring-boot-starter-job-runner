package ru.mynewtons.starter.job.runner;

public class SimpleInput implements Input<String> {

    private String value;

    @Override
    public String getValue() {
        return value;
    }
}
