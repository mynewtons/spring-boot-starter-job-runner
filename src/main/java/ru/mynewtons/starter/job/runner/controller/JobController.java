package ru.mynewtons.starter.job.runner.controller;

import com.fasterxml.jackson.core.JsonFactory;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.mynewtons.starter.job.runner.Runnable;
import ru.mynewtons.starter.job.runner.client.WorkflowClient;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.UUID;

@RestController
@RequestMapping("/api/jobs")
@Slf4j(topic = "🚴‍MyNewtons Job Runner Starter")
public class JobController {

    @Autowired
    private WorkflowClient workflowClient;

    @Autowired
    private ApplicationContext appContext;

    @PostMapping("/{job_name}/execute")
    public ResponseEntity execute(@PathVariable("job_name") String jobName,
                                  @RequestBody String input){
        Object bean = appContext.getBean(jobName);
        log.info("Get execute request...");
        if(bean instanceof Runnable){
            Runnable runner = (Runnable) bean;
            log.info("Runner found! " + jobName);
            Type type = ((ParameterizedType) runner.getClass().getGenericInterfaces()[0])
                    .getActualTypeArguments()[1];
            log.info("Try deserialize body...");
            log.info(input);
            Gson gson = new Gson();
            Object json = gson.fromJson(input, type);
            log.info("Deserialize complete...");
            UUID uuid = UUID.randomUUID();
            java.lang.Runnable task = () -> {
                log.info("Run new thread...");
                workflowClient.taskComplete(uuid.toString(),runner.execute(json));
                log.info("Job Runner complete task.");
            };
            Thread thread = new Thread(task);
            thread.start();

            return ResponseEntity.ok(uuid);
        }else {
            return ResponseEntity.badRequest().build();
        }
    }
}
